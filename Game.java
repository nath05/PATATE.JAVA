/**
 * main
 */

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Game extends JPanel {
  
  Patate patate = new Patate(this);

  public Game() {
		addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				patate.keyReleased(e);
			}

			@Override
			public void keyPressed(KeyEvent e) {
				patate.keyPressed(e);
			}
		});
		setFocusable(true);
	}

  public void paint(Graphics g) {
    super.paint(g);
    Graphics2D g2d = (Graphics2D) g;
    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
    RenderingHints.VALUE_ANTIALIAS_ON);
		patate.paint(g2d);
  }

  private void move() {
    patate.move();
  }
  
  public static void main(String[] args) throws InterruptedException {
    JFrame frame = new JFrame("PATATE");
    Game game = new Game();
    frame.add(game);
    frame.setSize(500, 500);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setVisible(true);

    while (true) {
			game.move();
			game.repaint();
			Thread.sleep(10);
		}
  }
}